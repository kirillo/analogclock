package com.evenmore.analogclock;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class AnalogClock extends View {
    private final Paint centerPinStyle;
    private final Paint labelStyle;
    private final Paint hoursHandStyle;
    private final Paint minutesHandStyle;
    private final Paint secondsHandStyle;

    private Calendar calendar;
    private final Timer timer;
    int hour;
    int minutes;
    int seconds;

    private int radius;
    private int centerX;
    private int centerY;

    private float padding;
    private int hoursHandLength;
    private int minutesHandLength;
    private int secondsHandLength;
    private float centerDotSize;
    private float userLabelSize;

    private int labelColor = Color.RED;
    private int handsColor = Color.BLACK;
    private int backgroundColor = Color.WHITE;

    private String dialLabel;
    private Rect dialLabelRect = new Rect();


    public AnalogClock(Context context) {
        this(context, null);
    }

    public AnalogClock(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AnalogClock(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.AnalogClock, 0, 0);
        try {
            userLabelSize = a.getDimension(R.styleable.AnalogClock_labelSize, 0);
            labelColor = a.getColor(R.styleable.AnalogClock_labelColor, Color.RED);
            handsColor = a.getInteger(R.styleable.AnalogClock_handColor, Color.BLACK);
            backgroundColor = a.getColor(R.styleable.AnalogClock_backgroundColor, Color.WHITE);
        } finally {
            a.recycle();
        }

        labelStyle = new Paint();
        labelStyle.setColor(labelColor);

        hoursHandStyle = new Paint();
        hoursHandStyle.setColor(handsColor);

        minutesHandStyle = new Paint();
        minutesHandStyle.setColor(handsColor);

        secondsHandStyle = new Paint();
        secondsHandStyle.setColor(handsColor);

        centerPinStyle = new Paint();
        centerPinStyle.setColor(handsColor);
        centerPinStyle.setStyle(Paint.Style.FILL_AND_STROKE);

        // update view every second
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.d("AnalogClock", "Tick");
                calendar = Calendar.getInstance();
                calendar.setTimeZone(TimeZone.getDefault());
                hour = calendar.get(Calendar.HOUR);
                minutes = calendar.get(Calendar.MINUTE);
                seconds = calendar.get(Calendar.SECOND);
                postInvalidate();
            }
        }, 0, 1000);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        timer.cancel();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        // clock radius
        radius = Math.min(width, height) / 2;

        // center of the clock
        centerX = width / 2;
        centerY = height / 2;

        // font size
        labelStyle.setTextSize(userLabelSize == 0 ? (float) (radius * 0.20) : userLabelSize);

        padding = labelStyle.getTextSize();

        // width of the clock hands
        hoursHandStyle.setStrokeWidth((float) (radius * 0.045));
        minutesHandStyle.setStrokeWidth((float) (radius * 0.03));
        secondsHandStyle.setStrokeWidth((float) (radius * 0.015));

        // length of the clock hands
        hoursHandLength = (int) ((radius - padding) * 0.6);
        minutesHandLength = (int) ((radius - padding) * 0.8);
        secondsHandLength = (int) ((radius - padding) * 0.8);

        // size of the center dot
        centerDotSize = (float) (radius * 0.1);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawColor(backgroundColor);
        canvas.drawCircle(centerX, centerY, centerDotSize, centerPinStyle);
        drawClockFace(canvas);
        drawHands(canvas);
    }

    private void drawClockFace(Canvas canvas) {
        for (int hour = 1; hour <= 12; hour++) {
            dialLabel = String.valueOf(hour);
            labelStyle.getTextBounds(dialLabel, 0, dialLabel.length(), dialLabelRect);

            canvas.drawText(dialLabel,
                    (float) (centerX - dialLabelRect.width() / 2 + (radius - padding) * Math.cos(Math.toRadians((hour * 30) - 90f))),
                    (float) (centerY + dialLabelRect.height() / 2 + (radius - padding) * Math.sin(Math.toRadians((hour * 30) - 90f))),
                    labelStyle);
        }
    }

    private void drawHands(Canvas canvas) {
        // hours
        // hour / 12f * 360f - hours
        // 30 * minutes / 60 - offset for minutes
        canvas.drawLine(centerX, centerY,
                (float) (centerX + (hoursHandLength) * Math.cos(Math.toRadians((hour / 12f * 360f + 30 * minutes / 60) - 90f))),
                (float) (centerY + (hoursHandLength) * Math.sin(Math.toRadians((hour / 12f * 360f + 30 * minutes / 60) - 90f))),
                hoursHandStyle);

        // minutes
        canvas.drawLine(centerX, centerY,
                (float) (centerX + (minutesHandLength) * Math.cos(Math.toRadians((minutes / 60.0f * 360.0f) - 90f))),
                (float) (centerY + (minutesHandLength) * Math.sin(Math.toRadians((minutes / 60.0f * 360.0f) - 90f))),
                minutesHandStyle);

        //seconds
        canvas.drawLine(centerX, centerY,
                (float) (centerX + (secondsHandLength) * Math.cos(Math.toRadians((seconds / 60.0f * 360.0f) - 90f))),
                (float) (centerY + (secondsHandLength) * Math.sin(Math.toRadians((seconds / 60.0f * 360.0f) - 90f))),
                secondsHandStyle);
    }
}
